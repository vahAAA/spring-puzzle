package com.puzzle.spring.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by VAHA on 18.05.2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomLog {
}
