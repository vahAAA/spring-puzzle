package com.puzzle.spring.component;

import com.puzzle.spring.annotation.CustomLog;
import com.puzzle.spring.annotation.LogPerformance;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.Proxy;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.lang.reflect.Field;


/**
 * Created by VAHA on 18.05.2017.
 */
public class LoggingHandleBeanPostProcessor implements BeanPostProcessor {
    private ArrayList<Class> annotatedClasses = new ArrayList<>();

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class beanClass = bean.getClass();

        ArrayList<Method> methods = getAllMethods(beanClass);
        // gets collection of methods for every bean, don't storing results of previously processed beans

        for (Method method : methods)
            if (method.isAnnotationPresent(CustomLog.class))
                annotatedClasses.add(beanClass);

        return bean;
    }

    private ArrayList<Method> getAllMethods(Class beanClass) {
        ArrayList<Method> result = new ArrayList<>();
        result.addAll(Arrays.asList(beanClass.getDeclaredMethods()));

        for (Class implementedInterface : beanClass.getInterfaces()) {
            result.addAll(Arrays.asList(implementedInterface.getMethods()));
            // getMethods() above  returns ALL methods from chain of inherited interfaces, no need of recursion
        }
        return result;
    }


    public Object postProcessAfterInitialization(final Object bean, String beanName) throws BeansException {
        Class beanClass = bean.getClass();

        if (annotatedClasses.contains(beanClass)) {
            Object proxy = java.lang.reflect.Proxy.newProxyInstance(bean.getClass().getClassLoader(),
                    bean.getClass().getInterfaces(), new java.lang.reflect.InvocationHandler() {
                        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                            System.out.println(method.getName() + " -- invocation started");
                            long time = System.nanoTime();
                            Object object = method.invoke(bean, args);
                            System.out.println(method.getName() + " -- invocation finished. Processing time: " + (System.nanoTime() - time));
                            return object;
                        }
                    });

            for (Field beanField : beanClass.getDeclaredFields()) {
                if ((beanField.getName().equalsIgnoreCase("arithmeticServiceObject"))) {  // Pure code here, but working :)
                    beanField.setAccessible(true);
                    ReflectionUtils.setField(beanField, bean, proxy);
                }
            }
            return proxy;
        }

        return bean;
    }
}

/*
    private Map<String, Class> map = new HashMap<String, Class>();

    public LoggingHandleBeanPostProcessor() {

    }

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();

        List<Method> methods = new ArrayList<>();

        for (Method method : methods) {
            if (method.isAnnotationPresent(CustomLog.class)) {
                map.put(beanName, beanClass);
            }
        }


        return bean;
    }

    public Object postProcessAfterInitialization(final Object bean, String beanName) throws BeansException {
        Class beanClass = map.get(beanName);
        if (beanClass != null) {
            return Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    System.out.println("start");
                    long before = System.nanoTime();
                    Object retVal = method.invoke(bean, args);
                    long after = System.nanoTime();
                    System.out.println(after - before);
                    System.out.println("finish");
                    return retVal;
                }
            });

        }
        return bean;
    }
}
*/