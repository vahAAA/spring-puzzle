package com.puzzle.spring.service;

import com.puzzle.spring.annotation.CustomLog;

public interface ArithmeticService {
    @CustomLog
    int add(int arg1, int arg2);

    @CustomLog
    int inc(int arg);

    //f = (arg1 + arg2) - (arg3+1)
    @CustomLog
    int function(int arg1, int arg2, int arg3);
}
